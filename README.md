# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This application displays list of popular movies and their details
* Version 1.0

### Architectural approach, tools used and things I have done ###

* Dependency injection only for repository data
* Unit testing for UI loading of movie detail fragment
* MVVM architecture used with livedata
* Modified retrofit OkHTTPClient used for API call
* GSON for parsing data from API
* Picasso for loading Images
* Whole code is written in kotlin
* Kotlin extension functions used for easy coding
* Added documentation for every activity, fragment, class, interface and function used

### Git structure ###

* Use master branch to create build and also for code review
* Created separated branches for creating pull request for easy management