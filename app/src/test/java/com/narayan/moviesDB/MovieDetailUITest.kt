package com.narayan.moviesDB

import android.os.Build
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentFactory
import androidx.fragment.app.testing.FragmentScenario
import androidx.lifecycle.Lifecycle
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.narayan.moviesDB.fragments.MovieDetailFragment
import org.hamcrest.Matchers
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config
import org.robolectric.annotation.LooperMode
import org.robolectric.shadows.ShadowLooper
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.Espresso
import androidx.test.espresso.assertion.ViewAssertions

@RunWith(AndroidJUnit4::class)
@LooperMode(LooperMode.Mode.PAUSED)
@Config(sdk = [Build.VERSION_CODES.LOLLIPOP_MR1])
class MovieDetailUITest {

    @Test
    fun testInvalidMovieId() {
        val fragmentFactory = object : FragmentFactory() {
            override fun instantiate(classLoader: ClassLoader, className: String): Fragment {
                return when (className) {
                    MovieDetailFragment::class.java.name -> {
                        MovieDetailFragment.newInstance(0)
                    }
                    else -> super.instantiate(classLoader, className)
                }
            }
        }
        val scenario = FragmentScenario.launchInContainer(
            fragmentClass = MovieDetailFragment::class.java,
            themeResId = R.style.Theme_MoviesDB,
            initialState = Lifecycle.State.RESUMED,
            factory = fragmentFactory)

        scenario.onFragment(object : FragmentScenario.FragmentAction<MovieDetailFragment> {
            override fun perform(fragment: MovieDetailFragment) {
                // Wait for product-verify response
                ShadowLooper.runUiThreadTasksIncludingDelayedTasks()
                Thread.sleep(1000)
                ShadowLooper.idleMainLooper()
                Espresso.onView(ViewMatchers.withId(R.id.no_data_view))
                    .check(ViewAssertions.matches(Matchers.not(ViewMatchers.isDisplayed())))
                scenario.moveToState(Lifecycle.State.DESTROYED)
            }
        })
    }

    @Test
    fun testValidMovieId() {
        val fragmentFactory = object : FragmentFactory() {
            override fun instantiate(classLoader: ClassLoader, className: String): Fragment {
                return when (className) {
                    MovieDetailFragment::class.java.name -> {
                        MovieDetailFragment.newInstance(550)
                    }
                    else -> super.instantiate(classLoader, className)
                }
            }
        }
        val scenario = FragmentScenario.launchInContainer(
            fragmentClass = MovieDetailFragment::class.java,
            themeResId = R.style.Theme_MoviesDB,
            initialState = Lifecycle.State.RESUMED,
            factory = fragmentFactory)

        scenario.onFragment(object : FragmentScenario.FragmentAction<MovieDetailFragment> {
            override fun perform(fragment: MovieDetailFragment) {
                // Wait for product-verify response
                ShadowLooper.runUiThreadTasksIncludingDelayedTasks()
                Thread.sleep(1000)
                ShadowLooper.idleMainLooper()
                Espresso.onView(ViewMatchers.withId(R.id.content_view))
                    .check(ViewAssertions.matches(Matchers.not(ViewMatchers.isDisplayed())))
                scenario.moveToState(Lifecycle.State.DESTROYED)
            }
        })
    }
}