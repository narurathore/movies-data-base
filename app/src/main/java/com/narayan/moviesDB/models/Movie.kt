package com.narayan.moviesDB.models

import com.google.gson.annotations.SerializedName
import com.narayan.moviesDB.utilities.AppConstants

/**
 * Data class for movie item
 */
data class Movie (

    @SerializedName("poster_path")
    val posterPath : String? = null,

    @SerializedName("original_title")
    val originalTitle: String? = null,

    @SerializedName("release_date")
    val releaseDate : String? = null,

    @SerializedName("id")
    val id : Int = 0,

    @SerializedName("vote_average")
    val voteAverage : Float = 0.0F

    ): BaseModel() {

    /**
     * Will return thumbnail url after adding base url to poster path
     * @return String
     */
    fun getThumbnailImageUrl() = AppConstants.BASE_IMAGE_URL + posterPath
}
