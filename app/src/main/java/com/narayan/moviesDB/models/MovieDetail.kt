package com.narayan.moviesDB.models

import com.google.gson.annotations.SerializedName
import com.narayan.moviesDB.utilities.AppConstants

/**
 * data class for movie detail response
 */
data class MovieDetail(

    @SerializedName("backdrop_path")
    val backdropPath : String,

    @SerializedName("original_title")
    val originalTitle :String,

    @SerializedName("overview")
    val overview: String,

    @SerializedName("poster_path")
    val posterPath : String,

    @SerializedName("tagline")
    val tagline : String,

    @SerializedName("status")
    val status : String,

    @SerializedName("release_date")
    val releaseDate : String,

    @SerializedName("vote_average")
    val voteAverage : Double

    ) : BaseModel() {

    /**
     * /**
     * Will return banner image url after adding base url to backdrop path
     * @return String
    */
     */
    fun getBannerImageURL() = AppConstants.BASE_IMAGE_URL + backdropPath

    /**
     * Will return thumbnail url after adding base url to poster path
     * @return String
     */
    fun getThumbNailImageURL() = AppConstants.BASE_IMAGE_URL + posterPath

    }
