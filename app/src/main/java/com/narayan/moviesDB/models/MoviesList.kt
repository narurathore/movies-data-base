package com.narayan.moviesDB.models

import com.google.gson.annotations.SerializedName

/**
 * dat class for list of movies response
 */
data class MoviesList(

    @SerializedName("results")
    val list: List<Movie>? = null

) : BaseModel()
