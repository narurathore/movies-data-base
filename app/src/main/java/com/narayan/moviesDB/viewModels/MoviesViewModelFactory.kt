package com.narayan.moviesDB.viewModels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.narayan.moviesDB.repository.RepositoryData
import kotlin.reflect.KFunction3

/**
 * View model factory to build view model according to view model type
 */
class MoviesViewModelFactory (private val builder: Builder): ViewModelProvider.Factory {

    private val errorMsg = "ViewModel Not Found"
    enum class Type {
        MOVIE_LIST, MOVIE_DETAIL
    }


    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        when(builder.type) {
            Type.MOVIE_LIST -> {
                return if (modelClass.isAssignableFrom(MoviesViewModel::class.java)) {
                    MoviesViewModel(builder.repositoryData!!) as T
                } else {
                    throw IllegalArgumentException(errorMsg)
                }
            }
            Type.MOVIE_DETAIL -> {
                return if (modelClass.isAssignableFrom(MoviesViewModel::class.java)) {
                    MoviesViewModel(builder.repositoryData!!, builder.movieId) as T
                } else {
                    throw IllegalArgumentException(errorMsg)
                }
            }
            else -> {
                throw RuntimeException("Cannot create an instance of $modelClass")
            }
        }
    }

    /**
     * Builder used for building view model
     */
    class Builder(var type: Type) {

        var movieId: Int = 0
            private set

        var repositoryData: RepositoryData? = null
            private set

        /**
         * Will set movie id
         */
        fun setMovieId(movieId: Int): Builder {
            this.movieId = movieId
            return this
        }

        /**
         * Will set repository data
         */
        fun setRepositoryData(repositoryData: RepositoryData): Builder {
            this.repositoryData = repositoryData
            return this
        }
    }

}