package com.narayan.moviesDB.viewModels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.narayan.moviesDB.apiHelper.APICallBackInterface
import com.narayan.moviesDB.utilities.AppConstants
import com.narayan.moviesDB.models.BaseModel
import com.narayan.moviesDB.models.MovieDetail
import com.narayan.moviesDB.models.MoviesList
import com.narayan.moviesDB.repository.RepositoryData

/**
 * View model to handle all data of movie list and movie details
 * @param repo = used to fetch data from repository
 * @param movieId = used to fetch movie detail
 */
class MoviesViewModel(private val repo:RepositoryData,private val movieId: Int? = null): ViewModel(), APICallBackInterface {

    var moviesListLiveData = MutableLiveData<MoviesList>()
    var movieDetailLiveData = MutableLiveData<MovieDetail>()
    var showProgressBar = MutableLiveData<Boolean>()
    var showRetryButton = MutableLiveData<Boolean>()

    /**
     * Will fetch movie list from repository
     */
    fun fetchMovies(){
        repo.fetchMoviesList(AppConstants.TAG_MOVIES_LIST, this)
        showProgressBar.value = true
    }

    /**
     * Will fetch movie detail from repository
     */
    fun fetchMovieDetail(){
        movieId?.let {
            repo.fetchMovieDetail(AppConstants.TAG_MOVIE_DETAIL, it, this)
            showProgressBar.value = true
        }
    }

    override fun onSuccessfulResponse(tag: String, response: BaseModel) {
        when (tag){
            AppConstants.TAG_MOVIES_LIST -> {
                if (response is MoviesList) {
                    moviesListLiveData.value = response
                }
            }
            AppConstants.TAG_MOVIE_DETAIL -> {
                if (response is MovieDetail) {
                    movieDetailLiveData.value = response
                }
            }
        }
        showProgressBar.value = false
    }

    override fun onErrorResponse(tag: String) {
        showProgressBar.value = false
        showRetryButton.value = true
    }
}