package com.narayan.moviesDB.viewHolders

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.narayan.moviesDB.R
import com.narayan.moviesDB.interfaces.MoviesClickListner
import com.narayan.moviesDB.models.Movie
import com.narayan.moviesDB.utilities.loadImage

/**
 * Movie view holder for recycler view
 * Will show image and title
 * @param itemView = Layout view
 * @param moviesClickListner = to handle click of movie item
 */
class MoviesViewHolder(itemView: View, val moviesClickListner: MoviesClickListner) : RecyclerView.ViewHolder(itemView) {

    val imageView = itemView.findViewById<ImageView>(R.id.thumbnail_image_view)
    val titleTV = itemView.findViewById<TextView>(R.id.title)

    /**
     * Will bind the movie to item view every time recycler calls bind function for particular item
     */
    fun onBind(movie: Movie){
        titleTV.text = movie.originalTitle
        imageView.loadImage(movie.getThumbnailImageUrl())
        itemView.setOnClickListener {
            moviesClickListner.onMoviesItemClick(movie)
        }
    }
}