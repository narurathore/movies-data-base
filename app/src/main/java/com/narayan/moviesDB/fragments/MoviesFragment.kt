package com.narayan.moviesDB.fragments

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.narayan.moviesDB.models.MoviesList
import com.narayan.moviesDB.R
import com.narayan.moviesDB.activities.MovieDetailActivity
import com.narayan.moviesDB.viewModels.MoviesViewModel
import com.narayan.moviesDB.adapter.MoviesRecyclerViewAdapter
import com.narayan.moviesDB.interfaces.MoviesClickListner
import com.narayan.moviesDB.models.Movie
import com.narayan.moviesDB.apiHelper.APIInterfaceModule
import com.narayan.moviesDB.repository.DaggerRepositoryDataComponent
import com.narayan.moviesDB.utilities.*
import com.narayan.moviesDB.viewModels.MoviesViewModelFactory
import kotlinx.android.synthetic.main.fragment_movies_item_list.*
import kotlinx.android.synthetic.main.fragment_movies_list.*
import kotlinx.android.synthetic.main.fragment_movies_list.progress_bar
import kotlinx.android.synthetic.main.no_data_view.*

/**
 * A fragment representing a list of movies.
 */
class MoviesFragment : Fragment(), MoviesClickListner, View.OnClickListener {

    private var columnCount = 3
    private lateinit var moviesViewModel: MoviesViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_movies_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewModel()
        retry_button.setOnClickListener(this)

        observe(moviesViewModel.moviesListLiveData, ::moviesRecieved)
        observe(moviesViewModel.showProgressBar, ::changeStateOfProgressBar)
        observe(moviesViewModel.showRetryButton, ::showNoDataWithRetryButton)
        moviesViewModel.fetchMovies()
    }

    /**
     * Will show data loading error with retry button to try again
     * @param shouldShow = flag used to show no data view
     */
    private fun showNoDataWithRetryButton(shouldShow: Boolean?) {
        if (shouldShow == true){
            no_data_view.visible()
            progress_bar.gone()
            recycler_view.gone()
        }
    }

    /**
     * Will show or hide progress bar according to flag given
     * @param showProgressBar = flag to show or hide progress bar
     */
    private fun changeStateOfProgressBar(showProgressBar: Boolean?) {
        if (showProgressBar == true){
            // show progress bar
            progress_bar.visible()
            recycler_view.gone()
            no_data_view.gone()
        } else {
            // hide progress bar
            progress_bar.gone()
        }
    }

    /**
     * Will initialize view model with initial data
     */
    private fun initViewModel(){
        val repoComponent= DaggerRepositoryDataComponent.builder()
            .aPIInterfaceModule(APIInterfaceModule(requireContext())).build()
        withViewModel<MoviesViewModel>(MoviesViewModelFactory(
            MoviesViewModelFactory.Builder(MoviesViewModelFactory.Type.MOVIE_LIST)
                .setRepositoryData(repoComponent.getRepository()))) {
            moviesViewModel = this
        }
    }

    /**
     * Will initialize recycler view adapter with movie list data
     */
    private fun moviesRecieved(moviesList: MoviesList?) {
        // Set the adapter
        moviesList?.list?.let {
            with(recycler_view) {
                layoutManager = when {
                    columnCount <= 1 -> LinearLayoutManager(context)
                    else -> GridLayoutManager(context, columnCount)
                }
                adapter = MoviesRecyclerViewAdapter(it, this@MoviesFragment)
                visible()
            }
        }
    }

    override fun onMoviesItemClick(movie: Movie) {
        //Open details screen
        val intent = Intent(context, MovieDetailActivity::class.java)
        intent.putExtra(AppConstants.INTENT_EXTRA_DATA_KEY_MOVIE_ID, movie.id)
        startActivity(intent)
    }

    override fun onClick(v: View?) {
        when (v?.id){
            retry_button.id -> {
                moviesViewModel.fetchMovies()
            }
        }
    }
}