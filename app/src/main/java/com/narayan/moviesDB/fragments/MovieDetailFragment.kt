package com.narayan.moviesDB.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.narayan.moviesDB.R
import com.narayan.moviesDB.models.MovieDetail
import com.narayan.moviesDB.apiHelper.APIInterfaceModule
import com.narayan.moviesDB.repository.DaggerRepositoryDataComponent
import com.narayan.moviesDB.utilities.*
import com.narayan.moviesDB.viewModels.MoviesViewModel
import com.narayan.moviesDB.viewModels.MoviesViewModelFactory
import kotlinx.android.synthetic.main.fragment_movie_detail.*
import kotlinx.android.synthetic.main.fragment_movie_detail_content_view.*
import kotlinx.android.synthetic.main.no_data_view.*

/**
 * A simple [Fragment] to display details of movie.
 * Use the [MovieDetailFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class MovieDetailFragment : Fragment(), View.OnClickListener {
    private var movieId: Int? = null
    private lateinit var moviesViewModel: MoviesViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // read arguments
        arguments?.let {
            movieId = it.getInt(AppConstants.INTENT_EXTRA_DATA_KEY_MOVIE_ID)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_movie_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // initializing view model
        initViewModel()

        // set click listeners
        retry_button.setOnClickListener(this)

        // set observers for view model data
        observe(moviesViewModel.movieDetailLiveData, ::showMovieDetails)
        observe(moviesViewModel.showProgressBar, ::changeStateOfProgressBar)
        observe(moviesViewModel.showRetryButton, ::showNoDataWithRetryButton)

        // Fetch movie details according to movie id from server
        moviesViewModel.fetchMovieDetail()
    }

    /**
     * Will show data loading error with retry button to try again
     * @param shouldShow = flag used to show no data view
     */
    private fun showNoDataWithRetryButton(shouldShow: Boolean?) {
        if (shouldShow == true){
            no_data_view.visible()
            progress_bar.gone()
            content_view.gone()
        }
    }

    /**
     * Will show or hide progress bar according to flag given
     * @param showProgressBar = flag to show or hide progress bar
     */
    private fun changeStateOfProgressBar(showProgressBar: Boolean?) {
        if (showProgressBar == true){
            // show progress bar
            progress_bar.visible()
            content_view.gone()
            no_data_view.gone()
        } else {
            // hide progress bar
            progress_bar.gone()
        }
    }

    /**
     * Will load movie details in UI components
     * @param movieDetail = details to be shown
     */
    private fun showMovieDetails(movieDetail: MovieDetail?) {
        // load movies data
        movieDetail?.let {
            backdrop_path.loadImage(it.getBannerImageURL(), R.drawable.banner_placeholder_image)
            poster_path.loadImage(it.getThumbNailImageURL())
            movie_name.text = it.originalTitle
            overview_detail.text = it.overview
            tagLineDetails.text = it.tagline
            statusDetails.text = it.status
            releaseDateDetails.text = it.releaseDate
            vote_average_detail.text = it.voteAverage.toString()
        }
        content_view.visible()
    }

    /**
     * Will initialize view model with initial data
     */
    private fun initViewModel(){
        val repoComponent= DaggerRepositoryDataComponent.builder()
            .aPIInterfaceModule(APIInterfaceModule(requireContext())).build()
        withViewModel<MoviesViewModel>(
            MoviesViewModelFactory(
            MoviesViewModelFactory.Builder(MoviesViewModelFactory.Type.MOVIE_DETAIL)
                .setRepositoryData(repoComponent.getRepository())
                .setMovieId(movieId?:0))
        ) {
            moviesViewModel = this
        }
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param movieId is id of selected movie to show details.
         * @return A new instance of fragment MovieDetailFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(movieId: Int) =
            MovieDetailFragment().apply {
                arguments = Bundle().apply {
                    putInt(AppConstants.INTENT_EXTRA_DATA_KEY_MOVIE_ID, movieId)
                }
            }
    }

    override fun onClick(v: View?) {
        when (v?.id){
            retry_button.id -> {
                moviesViewModel.fetchMovieDetail()
            }
        }
    }
}
