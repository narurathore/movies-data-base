package com.narayan.moviesDB.utilities

import android.graphics.drawable.Drawable
import android.view.View
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.*
import androidx.lifecycle.ViewModelProviders
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso

/**
 * Contains all common extensions used in project
 */

/**
 * Will make view visible
 */
fun View.visible() {
    visibility = View.VISIBLE
}

/**
 * Will make view gone
 */
fun View.gone() {
    visibility = View.GONE
}

/**
 * Will make view invisible
 */
fun View.invisible() {
    visibility = View.INVISIBLE
}

/**
 * Will load image in image view
 * @param url = url of image to load
 */
fun ImageView.loadImage(url: String?) {
    url?.let {
        Picasso.get().load(url).into(this)
    }
}

/**
 * Will load image in image view
 * @param url = url of image to load
 * @param placeHolder = drawable id of image used for placeholder
 */
fun ImageView.loadImage(url: String?, placeHolder: Int) {
    url?.let {
        Picasso.get().load(url).placeholder(placeHolder).into(this)
    }
}

/**
 * To observe live data
 */
fun <T : Any, L : LiveData<T>> LifecycleOwner.observe(liveData: L, body: (T?) -> Unit) {
    liveData.observe(this, Observer(body))
}

/**
 * To get view model
 */
inline fun <reified T : ViewModel> Fragment.getViewModel(viewModelFactory: ViewModelProvider.Factory): T {
    return ViewModelProviders.of(this, viewModelFactory)[T::class.java]
}

/**
 * To get view model
 */
inline fun <reified T : ViewModel> Fragment.withViewModel(viewModelFactory: ViewModelProvider.Factory, body: T.() -> Unit): T {
    val vm = getViewModel<T>(viewModelFactory)
    vm.body()
    return vm
}