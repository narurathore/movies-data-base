package com.narayan.moviesDB.utilities

/**
 * Contains all constants used by app
 */
class AppConstants {
    companion object {

        const val API_KEY = "6407bc9d855185eb79f3a9a6eba67742"

        const val TAG_MOVIES_LIST = "movies_list"
        const val TAG_MOVIE_DETAIL = "movie_detail"

        const val BASE_IMAGE_URL = "https://image.tmdb.org/t/p/w300"

        const val INTENT_EXTRA_DATA_KEY_MOVIE_ID = "movie_id"

    }
}