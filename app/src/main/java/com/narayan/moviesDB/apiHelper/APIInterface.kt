package com.narayan.moviesDB.apiHelper

import com.narayan.moviesDB.models.MovieDetail
import com.narayan.moviesDB.models.MoviesList
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Api interface for defining api urls and return model type
 */
interface APIInterface {

    @GET("/3/movie/popular")
    fun getMovies(@Query("api_key") api_key : String) : Call<MoviesList>


    @GET("/3/movie/{id}")
    fun getMovieDetails(@Path("id") id : String, @Query("api_key") api_key: String) : Call<MovieDetail>

}