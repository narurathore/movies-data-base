package com.narayan.moviesDB.apiHelper

import com.narayan.moviesDB.models.BaseModel

/**
 * Api call back interface for generic handling of response which will be returned through BaseModel
 */
interface APICallBackInterface {

    /**
     * To return success response
     */
    fun onSuccessfulResponse(tag: String, response: BaseModel)

    /**
     * To return error
     */
    fun onErrorResponse(tag: String)
}