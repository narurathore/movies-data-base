package com.narayan.moviesDB.apiHelper

import android.content.Context
import dagger.Module
import dagger.Provides

/**
 * Api interface module used for dependency injection purpose
 */
@Module
class APIInterfaceModule(val context: Context) {

    @Provides
    fun providesAPIInterface() : APIInterface {
        return APIClient.getRetrofit(context).create(APIInterface::class.java)
    }
}