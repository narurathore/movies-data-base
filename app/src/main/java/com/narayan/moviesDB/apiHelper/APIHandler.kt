package com.narayan.moviesDB.apiHelper

import android.util.Log
import com.narayan.moviesDB.models.BaseModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * API handler to make api handling generic via BaseModel class
 */
object APIHandler {

    /**
     * Will make api call with generic model type and handles response
     * @param tag = used to identify the request type
     * @param call = used to make api call with given model type
     * @param callback = used to return callbacks of success and failure
     */
    fun <T : BaseModel> makeApiCall(tag: String, call: Call<T>, callback: APICallBackInterface) {
        call.enqueue(object : Callback<T> {
            override fun onResponse(call: Call<T>, response: Response<T>) {
                if (response.isSuccessful) {
                    handleResponse(tag, response.body(), callback)
                } else{
                    Log.e("MarvelResponseError", response.message())
                    callback.onErrorResponse(tag)
                }
            }

            override fun onFailure(call: Call<T>, t: Throwable) {
                Log.e("Failure", t.message.toString())
                callback.onErrorResponse(tag)
            }

        })
    }

    /**
     * Will handle the response and return to caller
     */
    private fun handleResponse(tag: String, response: BaseModel?, callback: APICallBackInterface){
        if (response != null){
            callback.onSuccessfulResponse(tag, response)
        } else {
            callback.onErrorResponse(tag)
        }
    }

}