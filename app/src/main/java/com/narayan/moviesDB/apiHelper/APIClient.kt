package com.narayan.moviesDB.apiHelper

import android.content.Context
import android.net.ConnectivityManager
import okhttp3.Cache
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File

/**
 * Api client , which uses retrofit for api calling with cache handling
 */
object APIClient {

    private var retrofit: Retrofit? = null
    private val BASE_URL = "https://api.themoviedb.org"

    /**
     * Will return retrofit object initialized with cache http enabled http client
     */
    fun getRetrofit(context: Context): Retrofit {
        if (retrofit == null) {
            retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(getHTTPClient(context))
                .build()
        }
        return retrofit!!
    }

    /**
     * Will return OkHttpClient with cache enabled
     * @param context used for caching
     * @return OkHttpClient
     */
    private fun getHTTPClient(context: Context): OkHttpClient {
        val httpCacheDirectory = File(context.cacheDir, "responses")
        val cacheSize = 10 * 1024 * 1024 // 10 MiB
        val cache = Cache(httpCacheDirectory, cacheSize.toLong())
        val interceptor = Interceptor { chain ->
            val originalResponse: Response = chain.proceed(chain.request())
            if (isNetworkAvailable(context)) {
                val maxAge = 60 // read from cache for 1 minute
                originalResponse.newBuilder()
                    .header("Cache-Control", "public, max-age=$maxAge")
                    .build()
            } else {
                val maxStale = 60 * 60 * 24 * 28 // tolerate 4-weeks stale
                originalResponse.newBuilder()
                    .header("Cache-Control", "public, only-if-cached, max-stale=$maxStale")
                    .build()
            }
        }
        return OkHttpClient.Builder().addInterceptor(interceptor).cache(cache).build()
    }

    /**
     * Will return true if network is available
     * @param context
     * @return Boolean
     */
    private fun isNetworkAvailable(context: Context) : Boolean{
        return try {
            val ConnectMgr = context.getSystemService("connectivity") as ConnectivityManager
            run {
                val NetInfo = ConnectMgr.activeNetworkInfo
                NetInfo?.isConnected ?: false
            }
        } catch (var3: SecurityException) {
            false
        }
    }
}