package com.narayan.moviesDB.interfaces

import com.narayan.moviesDB.models.Movie

/**
 * Interface used for handling of movie item click
 */
interface MoviesClickListner {

    /**
     * function used to handle movie item click
     * @param movie = clicked movie
     */
    fun onMoviesItemClick(movie: Movie)
}