package com.narayan.moviesDB.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.narayan.moviesDB.R
import com.narayan.moviesDB.fragments.MovieDetailFragment
import com.narayan.moviesDB.utilities.AppConstants
import kotlinx.android.synthetic.main.activity_movie_detail.*

/**
 * Activity used to add fragment which will display movie detail
 */
class MovieDetailActivity : AppCompatActivity() {

    private var movieId :Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_detail)

        // Set up tool bar
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        // Read Intent Data
        readIntentData()

        // Add movie detail fragment
        addMovieDetailFragment()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    /**
     * Movie Id is read from intent data
     */
    private fun readIntentData(){
        movieId = intent.getIntExtra(AppConstants.INTENT_EXTRA_DATA_KEY_MOVIE_ID, 0)
        if (movieId == 0){
            finish()
        }
    }

    /**
     *  To add MovieDetailFragment in container
     */
    private fun addMovieDetailFragment() {
        supportFragmentManager.beginTransaction()
            .replace(R.id.movie_detail_fragment_container, MovieDetailFragment.newInstance(movieId),
                MovieDetailFragment::class.java.name)
            .commitAllowingStateLoss()
    }
}