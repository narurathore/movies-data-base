package com.narayan.moviesDB.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.narayan.moviesDB.R
import com.narayan.moviesDB.fragments.MoviesFragment
import kotlinx.android.synthetic.main.activity_main.*

/**
 * Activity is used to add fragment which will display list of movies
 */
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //set support toolbar
        setSupportActionBar(toolbar)

        //add movies list fragment
        addMoviesFragment()
    }

    /**
     * To add MoviesFragment in fragment container
     */
    private fun addMoviesFragment() {
        supportFragmentManager.beginTransaction()
            .replace(R.id.movies_fragment_container, MoviesFragment(), MoviesFragment::class.java.name)
            .commitAllowingStateLoss()
    }
}