package com.narayan.moviesDB.repository

import com.narayan.moviesDB.apiHelper.APICallBackInterface
import com.narayan.moviesDB.apiHelper.APIHandler
import com.narayan.moviesDB.apiHelper.APIInterface
import com.narayan.moviesDB.models.MovieDetail
import com.narayan.moviesDB.models.MoviesList
import com.narayan.moviesDB.utilities.AppConstants
import javax.inject.Inject

/**
 * Repository to create and handle api call to remote server
 * @param apiInterface = interface used for creating api request
 */
class RepositoryData @Inject constructor(val apiInterface: APIInterface){

    /**
     * Will fetch movie list from server
     * @param tag = request type
     * @param callback = callback for success or failure of request
     */
    fun fetchMoviesList(tag: String,callback: APICallBackInterface) {
        APIHandler.makeApiCall(
            tag, apiInterface.getMovies(AppConstants.API_KEY),
            callback)
    }

    /**
     * Will fetch movie detail from server
     * @param tag = request type
     * @param movieId = id of movie detail to be fetched
     * @param callback = callback for success or failure of request
     */
    fun fetchMovieDetail(tag: String, movieId: Int, callback: APICallBackInterface) {
        APIHandler.makeApiCall(
            tag, apiInterface.getMovieDetails(movieId.toString(),
                AppConstants.API_KEY), callback)
    }

}