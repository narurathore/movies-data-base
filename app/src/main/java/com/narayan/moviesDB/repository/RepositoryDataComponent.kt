package com.narayan.moviesDB.repository

import com.narayan.moviesDB.apiHelper.APIInterfaceModule
import dagger.Component

/**
 * Dependency injection component for Repository Data
 */
@Component (modules = [APIInterfaceModule::class])
interface RepositoryDataComponent {

    /**
     * Will return repository data after creating from dependency injection
     */
    fun getRepository(): RepositoryData
}