package com.narayan.moviesDB.adapter

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.narayan.moviesDB.models.Movie
import com.narayan.moviesDB.R
import com.narayan.moviesDB.interfaces.MoviesClickListner

import com.narayan.moviesDB.viewHolders.MoviesViewHolder

/**
 * [RecyclerView.Adapter] that can display a [Movie].
 * @param values = is list of movies to be displayed in recycler view
 * @param moviesClickListner = to handle click of recycler view item
 */
class MoviesRecyclerViewAdapter(
    private val values: List<Movie>,
    private val moviesClickListner: MoviesClickListner
) : RecyclerView.Adapter<MoviesViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MoviesViewHolder {

        return MoviesViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_movies_item, parent, false), moviesClickListner)

    }

    override fun onBindViewHolder(holder: MoviesViewHolder, position: Int) {
        holder.onBind(values[position])
    }

    override fun getItemCount(): Int = values.size

}